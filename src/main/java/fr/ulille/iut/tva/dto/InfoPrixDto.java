package fr.ulille.iut.tva.dto;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InfoPrixDto extends InfoTauxDto{
	
	private double montantTotal;
	private double montantTva;
	private double somme;
	
	public InfoPrixDto() {}
	
	public InfoPrixDto(String label, double taux, double somme) {
		super(label, taux);
		this.somme = somme;
		this.montantTva = taux / 100.0 * somme;
		this.montantTotal = somme + montantTva;
	}

	public double getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}

	public double getMontantTva() {
		return montantTva;
	}

	public void setMontantTva(double montantTva) {
		this.montantTva = montantTva;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(double somme) {
		this.somme = somme;
	}

}
